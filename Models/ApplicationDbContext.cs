﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookListMVC.Models
{
    //initializing a constructor ans DB context.
    public class ApplicationDbContext : DbContext //inheretating the DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        //we need to add our Book to the database so whenever we add a table, we will add that proterty inside the application DbContext

        public DbSet<Book> Books { get; set; }

    }
}
